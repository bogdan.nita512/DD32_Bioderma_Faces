import TQ_01_01_L from "./TripleQuote_01_01_Left.png";
import TQ_01_01_R from "./TripleQuote_01_01_Right.png";

import TQ_01_02_L from "./TripleQuote_01_02_Left.png";
import TQ_01_02_R from "./TripleQuote_01_02_Right.png";

import TQ_01_03_L from "./TripleQuote_01_03_Left.png";
import TQ_01_03_R from "./TripleQuote_01_03_Right.png";

import { QuotePair } from "../Single";

export type TripleQuote = {
    first: QuotePair,
    second: QuotePair,
    third: QuotePair
}

export const tripleQuotes = [
    {
        first: { left: TQ_01_01_L, right: TQ_01_01_R, },
        second: { left: TQ_01_02_L, right: TQ_01_02_R, },
        third: { left: TQ_01_03_L, right: TQ_01_03_R, },
    }

]