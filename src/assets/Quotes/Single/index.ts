import SQ_01_R from "./SingleQuote_01_Right.png";
import SQ_01_L from "./SingleQuote_01_Left.png";

import SQ_02_R from "./SingleQuote_02_Right.png";
import SQ_02_L from "./SingleQuote_02_Left.png";

import SQ_03_R from "./SingleQuote_03_Right.png";
import SQ_03_L from "./SingleQuote_03_Left.png";

import SQ_04_R from "./SingleQuote_04_Right.png";
import SQ_04_L from "./SingleQuote_04_Left.png";

import SQ_05_R from "./SingleQuote_05_Right.png";
import SQ_05_L from "./SingleQuote_05_Left.png";

import SQ_06_R from "./SingleQuote_06_Right.png";
import SQ_06_L from "./SingleQuote_06_Left.png";

import SQ_07_R from "./SingleQuote_07_Right.png";
import SQ_07_L from "./SingleQuote_07_Left.png";

export type QuotePair = {
    left: string,
    right: string,
}

export const singleQuotes = [
    { left: SQ_01_L, right: SQ_01_R, },
    { left: SQ_02_L, right: SQ_02_R, },
    { left: SQ_03_L, right: SQ_03_R, },
    { left: SQ_04_L, right: SQ_04_R, },
    { left: SQ_05_L, right: SQ_05_R, },
    { left: SQ_06_L, right: SQ_06_R, },
    { left: SQ_07_L, right: SQ_07_R, },
] as QuotePair[];