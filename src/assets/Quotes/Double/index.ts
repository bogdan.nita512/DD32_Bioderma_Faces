import DQ_01_01_R from "./DoubleQuote_01_01_Right.png";
import DQ_01_01_L from "./DoubleQuote_01_01_Left.png";
import DQ_01_02_R from "./DoubleQuote_01_02_Right.png";
import DQ_01_02_L from "./DoubleQuote_01_02_Left.png";

import DQ_02_01_R from "./DoubleQuote_02_01_Right.png";
import DQ_02_01_L from "./DoubleQuote_02_01_Left.png";
import DQ_02_02_R from "./DoubleQuote_02_02_Right.png";
import DQ_02_02_L from "./DoubleQuote_02_02_Left.png";

import DQ_03_01_R from "./DoubleQuote_03_01_Right.png";
import DQ_03_01_L from "./DoubleQuote_03_01_Left.png";
import DQ_03_02_R from "./DoubleQuote_03_02_Right.png";
import DQ_03_02_L from "./DoubleQuote_03_02_Left.png";

import DQ_04_01_R from "./DoubleQuote_04_01_Right.png";
import DQ_04_01_L from "./DoubleQuote_04_01_Left.png";
import DQ_04_02_R from "./DoubleQuote_04_02_Right.png";
import DQ_04_02_L from "./DoubleQuote_04_02_Left.png";

import DQ_05_01_R from "./DoubleQuote_05_01_Right.png";
import DQ_05_01_L from "./DoubleQuote_05_01_Left.png";
import DQ_05_02_R from "./DoubleQuote_05_02_Right.png";
import DQ_05_02_L from "./DoubleQuote_05_02_Left.png";

import DQ_06_01_R from "./DoubleQuote_06_01_Right.png";
import DQ_06_01_L from "./DoubleQuote_06_01_Left.png";
import DQ_06_02_R from "./DoubleQuote_06_02_Right.png";
import DQ_06_02_L from "./DoubleQuote_06_02_Left.png";

import DQ_07_01_R from "./DoubleQuote_07_01_Right.png";
import DQ_07_01_L from "./DoubleQuote_07_01_Left.png";
import DQ_07_02_R from "./DoubleQuote_07_02_Right.png";
import DQ_07_02_L from "./DoubleQuote_07_02_Left.png";

import { QuotePair } from "../Single";

export type DoubleQuote = {
    first: QuotePair,
    second: QuotePair
}

export const doubleQuotes = [
    {
        first: { left: DQ_01_01_L, right: DQ_01_01_R, },
        second: { left: DQ_01_02_L, right: DQ_01_02_R, },
    },
    {
        first: { left: DQ_02_01_L, right: DQ_02_01_R, },
        second: { left: DQ_02_02_L, right: DQ_02_02_R, },
    },
    {
        first: { left: DQ_03_01_L, right: DQ_03_01_R, },
        second: { left: DQ_03_02_L, right: DQ_03_02_R, },
    },
    {
        first: { left: DQ_04_01_L, right: DQ_04_01_R, },
        second: { left: DQ_04_02_L, right: DQ_04_02_R, },
    },
    {
        first: { left: DQ_05_01_L, right: DQ_05_01_R, },
        second: { left: DQ_05_02_L, right: DQ_05_02_R, },
    },
    {
        first: { left: DQ_06_01_L, right: DQ_06_01_R, },
        second: { left: DQ_06_02_L, right: DQ_06_02_R, },
    },
    {
        first: { left: DQ_07_01_L, right: DQ_07_01_R, },
        second: { left: DQ_07_02_L, right: DQ_07_02_R, },
    },
] as DoubleQuote[];