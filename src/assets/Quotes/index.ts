import { doubleQuotes as double } from "./Double";
import { singleQuotes as single } from "./Single";
import { tripleQuotes as triple } from "./Triple";

export const quotes = { single, double, triple }