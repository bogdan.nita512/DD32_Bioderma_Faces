import { Component, createMemo, createSignal, onMount } from 'solid-js';
import { saneDefaults, useHuman } from './hooks/useHuman';
import { Webcam } from "solid-webcam";
import { Config } from '@vladmandic/human';
import Background from "./assets/Background.svg?component-solid"
// import Overlay from "./assets/Overlay.svg?component-solid"
import Overlay from "./assets/Overlay.png"
import { Key } from '@solid-primitives/keyed';
import { ReactiveMap } from '@solid-primitives/map';
import { createShortcut } from '@solid-primitives/keyboard';
import { Quote, QuoteProvider } from './components/Quote';
import { Scheduled } from '@solid-primitives/scheduled';

const constraints = {
	aspectRatio: { ideal: 16 / 9 },
	width: { ideal: 1920 },
	height: { ideal: 1080 },
	facingMode: "user"
} as MediaTrackConstraints;

export const config: Partial<Config> =
{
	debug: false,
	backend: "webgl",
	modelBasePath: 'https://cdn.jsdelivr.net/gh/vladmandic/human-models/models/',

	filter: { ...saneDefaults.filter, enabled: true },
	face: {
		...saneDefaults.face,
		enabled: true,
		iris: { enabled: true },
		mesh: { enabled: true },
		detector: {
			...saneDefaults.face.detector,
			enabled: true,
			maxDetected: 5,
			minConfidence: 0.5,
		},
	},
}


export const App: Component = () => {
	const api = useHuman({ config })
	onMount(() => api.start())
	const toggleHuman = createMemo(() => api.running() ? api.stop : api.start)

	const [debug, setDebug] = createSignal(false);
	createShortcut(
		["Shift", "~"],
		() => {
			setDebug(value => !value)
			console.log("Shif + ~ pressed: " + debug())
		},
		{ preventDefault: false },
	);


	return (
		<div class="background h-screen w-screen flex justify-center items-center overflow-hidden">
			<img src={Overlay} class='absolute z-30 bottom-0 inset-x-0 w-full pointer-events-none' />
			<div class='h-5/6 z-20 aspect-video '>
				<Webcam
					ref={api.setVideo}
					audio={false}
					class="container rounded-lg shadow-lg"
					preload="auto"
					videoConstraints={constraints}
					onclick={toggleHuman()} />
				<QuoteProvider>
					<Key each={api.result()?.face} by={(face) => face.id}>
						{(face, _) => <Quote face={face} debug={debug} video={api.video} />}
					</Key>
				</QuoteProvider>
			</div>
			{/* <div class='fixed inset-x-0 bottom-0 p-4 z-20 text-black text-sm text-center font-normal antialiased'>
				<div>PP-GZ-RO-0113</div>
				<div>Acest material este destinat profesioniștilor în domeniul sănătății.</div>
			</div> */}
		</div >
	);
};

