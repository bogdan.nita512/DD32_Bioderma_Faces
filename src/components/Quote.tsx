import { createContextProvider } from "@solid-primitives/context";
import { ReactiveMap } from "@solid-primitives/map";
import { debounce, Scheduled } from "@solid-primitives/scheduled";
import { FaceResult } from "@vladmandic/human";
import { chunk, map, sample } from "lodash";
import { Accessor, Component, createEffect, createMemo, createSignal, onCleanup, onMount, Show } from "solid-js";
import { quotes } from "../assets/Quotes";
import { QuotePair } from "../assets/Quotes/Single";

// const toURL = (module: () => Promise<string>) => new URL(module.name, import.meta.url).href;
// const quotes = {
//     single: chunk(map(import.meta.glob<string>('../assets/Quotes/Single/*.png'), toURL), 2),
//     double: chunk(chunk(map(import.meta.glob<string>('../assets/Quotes/Double/*.png'), toURL), 2), 2),
//     triple: chunk(chunk(map(import.meta.glob<string>('../assets/Quotes/Triple/*.png'), toURL), 2), 3),
// }

console.log(quotes);

const toPixel = (value: number) => `${value}px`
const toPercentage = (value: number) => `${value * 100}%`

type Entry = {
    variation: QuotePair,
    dispose: Scheduled<[]>
};
export const [QuoteProvider, useQuoteContext] = createContextProvider((props: {}) => {

    const context = new ReactiveMap<number, Entry>();
    return { context };
});

export const Quote: Component<{
    debug?: Accessor<boolean>
    face: Accessor<FaceResult>
    video: Accessor<HTMLVideoElement>
}> =
    (props) => {
        const debug = createMemo(() => props.debug() ?? false)

        const { context } = useQuoteContext();
        const instanceId = createMemo(() => props.face().id);
        // const siblingId = createMemo(() => props.face().id - 1);
        const siblingsId = createMemo(() => ({
            primary: props.face().id - 1,
            secondary: props.face().id - 1,
        }))

        const hasInstance = createMemo(() => context?.has(instanceId()) ?? false)
        // const hasSibling = createMemo(() => context?.has(siblingId()) ?? false)
        const hasSiblings = createMemo(() => ({
            primary: context?.has(siblingsId().primary) ?? false,
            secondary: context?.has(siblingsId().secondary) ?? false,
        }))

        const instance = createMemo(() => context?.get(instanceId()));
        // const sibling = createMemo(() => context?.get(siblingId()))
        const siblings = createMemo(() => ({
            primary: context?.get(siblingsId().primary),
            secondary: context?.get(siblingsId().primary),
        }))

        const dispose = debounce(() => context.delete(instanceId()), 1250)

        onMount(() => {
            if (hasInstance()) {
                instance().dispose.clear();
            }
            else if (instanceId() % 3 == 1 && hasSiblings().primary && hasSiblings().secondary) {
                const quote = sample(quotes.triple);
                context.set(instanceId(), { dispose, variation: quote.first })
                context.set(siblingsId().primary, { ...siblings().primary, variation: quote.second })
                context.set(siblingsId().secondary, { ...siblings().secondary, variation: quote.first })
            }
            else if (instanceId() % 2 == 1 && hasSiblings().primary) {
                const quote = sample(quotes.double);
                context.set(instanceId(), { dispose, variation: quote.first })
                context.set(siblingsId().primary, { ...siblings().primary, variation: quote.second })
            }
            else {
                const quote = sample(quotes.single);
                context.set(instanceId(), { dispose, variation: quote })
            }
        })

        onCleanup(() => {

            if (instanceId() % 3 == 1 && hasSiblings().primary && hasSiblings().secondary) {
                const quote = sample(quotes.double);
                context.set(siblingsId().primary, { ...siblings().primary, variation: quote.first })
                context.set(siblingsId().secondary, { ...siblings().secondary, variation: quote.second })
            }
            else if (instanceId() % 2 == 1 && hasSiblings().primary) {
                const quote = sample(quotes.single);
                context.set(siblingsId().primary, { ...siblings().primary, variation: quote })
            }

            if (hasInstance()) { instance().dispose() }
        })

        const [anchor, setAnchor] = createSignal<HTMLDivElement>();
        const [content, setContent] = createSignal<HTMLImageElement>();

        const [direction, setDirection] = createSignal<"right" | "left">("right");

        createEffect(() => {
            if (props.video() && anchor() && content()) {
                const videoRect = props.video().getBoundingClientRect();
                const detection = new DOMRect(...props.face().boxRaw);

                detection.x = videoRect.x + detection.x * videoRect.width;
                detection.y = videoRect.y + detection.y * videoRect.height;
                detection.width *= videoRect.width;
                detection.height *= videoRect.height;

                const contentRect = new DOMRect(
                    detection.right,
                    detection.top - detection.height / 2,
                    detection.width,
                    detection.height,
                )

                if (contentRect.top < 0) {
                    contentRect.y += 0 - contentRect.top;
                }
                if (contentRect.bottom > window.innerHeight) {
                    contentRect.y -= contentRect.bottom - window.innerHeight;
                }

                // if (contentRect.left < 0) {
                //     // contentRect.x += 0 - contentRect.left;
                //     contentRect.x = detection.right
                //     // contentRect.width = -detection.width
                //     setDirection("right");
                //     console.log("right");
                // }
                // else if (contentRect.right > window.innerWidth && direction() == "right") {
                //     // contentRect.x -= contentRect.right - window.innerWidth;
                //     console.log(contentRect);
                //     contentRect.x = detection.bottom;
                //     contentRect.width = -detection.width;
                //     console.log(contentRect);
                //     setDirection("left");
                //     console.log(contentRect);
                // }



                anchor().style.top = toPixel(detection.top);
                anchor().style.left = toPixel(detection.left);
                anchor().style.width = toPixel(detection.width);
                anchor().style.height = toPixel(detection.height);

                content().style.top = toPixel(contentRect.top);
                content().style.left = toPixel(contentRect.left);
                content().style.width = toPixel(contentRect.width);
                content().style.height = toPixel(contentRect.height);
            }
        })

        return (
            <>
                <div
                    ref={setAnchor}
                    class="fixed border-8 border-dashed rounded-xl z-20"
                    classList={{ "invisible": !debug() }}
                />
                <Show when={hasInstance()}>
                    <img
                        class="fixed object-contain"
                        ref={setContent}
                        src={direction() == "right" ? instance().variation.right : instance().variation.left}
                        style={{ "max-width": "25vw", "min-width": "5vw", }} />
                </Show>

            </>
        )
    }